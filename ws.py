import os,bottle;app=bottle.default_app()
class App(bottle.Bottle):
    def get_user(app):
        #print("GU", tokens.get(bottle.request.headers.get('T','')))
        return tokens.get(bottle.request.headers.get('T',''))
    #def is_anon(app): return app.user==tokens['']
    def render_tmpl(app,path,d={}):
        for role in app.get_user()['roles']:
            filename = './'+role+path
            if os.path.exists(filename):
                return bottle.jinja2_template(filename, d)
            pass
        raise bottle.HTTPError(404, 'Not Found')
        return bottle.HTTPError(404)
        raise bottle.HTTPError(404, 'Not Found')
    def render_file(app,path):
        for role in app.get_user()['roles']:
            dirname  = './'+role
            if os.path.exists(dirname+path):
                return bottle.static_file(path,dirname)
            pass
        raise bottle.HTTPError(404, 'Not Found')
        return bottle.HTTPError(404)
    pass
app = App()
role, emails, tokens, users = 'public', dict(), dict(), [dict(
    email="unknown@anonymous.com",
    token='',
),dict(
    email="guest@visitor.com",
    password="visitor",
    roles=['guest'],
    token='t1',
),dict(
    email="ozzy@osbourne.com",
    password="bat",
    roles=['user'],
)]
for u in users:
    u.setdefault('token', '')
    u.setdefault('roles', [])
    u['roles'].insert(0, role)
    if u!=users[0]:
        if token := u['token']:       tokens[token] = u
        emails[u['email']+u['password']] = u
    else:           u['token'] = '' ; tokens[''+''] = u
    pass
#app.get_user = lambda:tokens.get(bottle.request.headers.get('T',''))
#app.is_anon=lambda:app.user==tokens['']
'''
def render_tmpl(path,d={}):
    for u in app.get_user():
        filename = './'+u['role']+path
        if os.path.exists(filename):
            return bottle.jinja2_template(filename, d)
        pass
    raise HTTPError(404)
def render_file(path):
    for u in app.get_user():
        dirname  = './'+u['role']
        if os.path.exists(dirname+path):
            return bottle.static_file(path,dirname)
        pass
    raise HTTPError(404)
'''
#app.render_tmpl=render_tmpl
#app.render_file=render_file
def add_default_headers(defaults={
        'Content-Language': 'en-US',
        'Cache-Control': 'no-cache, no-store, must-revalidate',
        'Pragma': 'no-cache',
        'Expires': '0',
}): [bottle.response.set_header(k, v) for k,v in defaults.items()]
@app.get('/logout')
def _():
    add_default_headers()
    u = app.get_user()
    #print("U", u)
    if not app.is_anon():
        del token[u['token']]
        u['token'] = ''
        pass
    bottle.response.set_header('T', '')
    return dict(result=True)
@app.get('/login')
def _():
    add_default_headers()
    if t := bottle.request.GET('T'):
        bottle.response.set_header('T', t)
        return dict(result=True)
    k = bottle.request.GET('email') + bottle.request.GET('password')
    return dict(result=False)
@app.get('/')
@app.get('<path:path><ext:re:\\.html>')
@app.get('<path:path>/')
def __(path='', ext='/index.html'):
    add_default_headers()
    u = app.get_user()
    print("U", u)
    print("U", u['roles'])
    return app.render_tmpl(path+ext, dict(GET=dict(bottle.request.GET)))
@app.get('<path:path>')
def __(path):
    add_default_headers()
    u = app.get_user()
    print("U", u)
    print("U", u['roles'])
    return app.render_file(path)
