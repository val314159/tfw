call:: clean all
all:: ;python -umbottle ws:app -b:9009 --debug --reload
clean:: _clean tree
_clean::
	find . -name '*~' -o -name '.*~' | xargs rm -fr
	rm -fr __pycache__ node_modules dist .parcel-cache
	rm -fr yarn.lock bun.lockb package-lock.json
tree:;	tree -a -I .git
