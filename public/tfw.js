const app=new function TinyFrameWork(){
    const self=this, _=Object.getPrototypeOf(self)
    //_.error404 = "<div style='background: pink'>404 Not Found</div>"
    _.qsa=x=>document.querySelectorAll(x)
    _.qs =x=>document.querySelector(x)
    _.ga=(e,a)=>e.getAttribute(a)
    _.ra=(e,a)=>(e.removeAttribute(a),e)
    _.prevent=e=>(e.preventDefault(),e)
    _.search=(a,b,c)=>{let d=a.search(b,c);if(d!==-1)return d;throw'TE'}  
    _.evalstr=t=>{
	let b=t.search('<'+'script>')+8
	if(b!==7)window.eval(t.substring(b,_.search(t,'</'+'script>',b)))}
    _.text=async r=>r.ok?await r.text():_.error404?_.error404:await r.text()
    _.click=async s=>_.evalstr(_.qs(_.ga(s,'@')).innerHTML =
			       await _.text(await fetch(_.ga(s,'href'))))
    _.onclick=e=>_.click(_.prevent(e).srcElement)
    _.rm_star_click=x=>_.click(_.ra(x,'*'))
    _.connect=x=>!x.onclick&&_.ga(x,'@')&&(x.onclick=_.onclick)
    _.hookup=()=>(_.qsa('[\\*]' ).forEach(_.rm_star_click) ||
		  _.qsa('[href]').forEach(_.connect))}
